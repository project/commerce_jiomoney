DESCRIPTION
===========

Provides reliance jio money payment gateway integration with Commerce

commerce_reliancejiomoney.module : the core API module


INSTALLATION
============

1. Drop into your preferred modules directory
2. Enable the module from admin/build/modules
3. Goto admin/commerce/config/payment-methods.
4. Edit Action and Configure the Working Key & Merchant ID.
